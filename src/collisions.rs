use std::ffi::OsStr;
use std::ffi::OsString;
use std::fmt;
use std::path::PathBuf;

pub struct Collision {
    source: PathBuf,
    dest: PathBuf,
}

impl Collision {
    pub fn new(source: PathBuf, dest: PathBuf) -> Self {
        let common_ancestor = promote::nonroot_common_ancestor(&source, &dest).unwrap();
        let source = source.strip_prefix(&common_ancestor).unwrap().to_path_buf();
        let dest = dest.strip_prefix(&common_ancestor).unwrap().to_path_buf();
        Self { source, dest }
    }
}

impl fmt::Display for Collision {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} ⊶─ {}", self.source.display(), self.dest.display())
    }
}

pub struct HardCollisions {
    collisions: Vec<Collision>,
}

impl HardCollisions {
    pub fn new() -> Self {
        Self {
            collisions: Vec::new(),
        }
    }

    pub fn push(&mut self, source: PathBuf, dest: PathBuf) {
        self.collisions.push(Collision::new(source, dest));
    }

    pub fn finalize(self) -> Option<Self> {
        if self.collisions.is_empty() {
            None
        } else {
            Some(self)
        }
    }
}

impl fmt::Display for HardCollisions {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for collision in &self.collisions {
            writeln!(f, "{}", collision)?;
        }
        Ok(())
    }
}

fn evolve(file_name: &OsStr) -> OsString {
    let mut os_string = file_name.to_os_string();
    os_string.push("%");
    os_string
}

pub struct SoftCollision {
    current_path: PathBuf,
    temp_path: PathBuf,
    desired_path: PathBuf,
}

impl SoftCollision {
    pub fn new(current_path: PathBuf, desired_path: PathBuf) -> Self {
        let temp_file_name = evolve(desired_path.file_name().unwrap());
        let mut temp_path = desired_path.with_file_name(&temp_file_name);
        while temp_path.exists() {
            let temp_file_name = evolve(&temp_file_name);
            temp_path = desired_path.with_file_name(temp_file_name);
        }
        let temp_path = desired_path.with_file_name(temp_file_name);
        Self {
            current_path,
            temp_path,
            desired_path,
        }
    }

    pub fn resolve(self) -> std::io::Result<()> {
        std::fs::rename(&self.current_path, &self.temp_path)?;
        for ancestor in self.current_path.ancestors().skip(1) {
            if ancestor == self.desired_path.parent().unwrap()
                || std::fs::remove_dir(ancestor).is_err()
            {
                break;
            }
        }
        std::fs::rename(self.temp_path, self.desired_path)?;
        Ok(())
    }
}
