use std::{
    fs,
    path::{Path, PathBuf},
};

use promote::Pedigree;

use crate::collisions::{HardCollisions, SoftCollision};

pub struct Mover {
    rename: bool,
    overwrite: bool,
}

impl Mover {
    pub fn new(rename: bool, overwrite: bool) -> Self {
        Self { rename, overwrite }
    }

    pub fn relocate(
        &self,
        promdirs: &[PathBuf],
        destdir: &Path,
    ) -> std::io::Result<Option<HardCollisions>> {
        let mut soft_collisions = Vec::new();
        let mut hard_collisions = HardCollisions::new();

        for dir in promdirs {
            for entry in fs::read_dir(dir)? {
                let entry = entry?;
                let mut dest = destdir.join(entry.file_name());
                if dest.exists() {
                    if dir.starts_with(&dest) {
                        soft_collisions.push(SoftCollision::new(entry.path(), dest));
                        continue;
                    }
                    if self.rename {
                        let mut counter = 0;
                        let path = entry.path();
                        let ext = path.extension();
                        while dest.exists() {
                            let file_stem = path.file_stem().unwrap();
                            let mut try_filename = file_stem.to_os_string();
                            try_filename.push("~");
                            try_filename.push(counter.to_string());
                            if let Some(ext) = ext {
                                try_filename.push(".");
                                try_filename.push(ext);
                            }
                            dest = destdir.join(try_filename);
                            counter += 1;
                        }
                    } else if !self.overwrite {
                        hard_collisions.push(entry.path(), dest);
                        continue;
                    }
                }
                fs::rename(entry.path(), dest)?;
            }
            if std::fs::read_dir(dir)?.next().is_none() {
                std::fs::remove_dir(dir)?;
                // remove the barren branch upto the destdir
                if destdir.is_ancestor_of(dir) {
                    let mut dir = dir.as_path();
                    while let Some(parent) = dir.parent()
                        && destdir.is_ancestor_of(parent)
                    {
                        match std::fs::read_dir(parent)?.next() {
                            None => {
                                std::fs::remove_dir(parent)?;
                                dir = parent;
                            }
                            Some(_) => break,
                        }
                    }
                }
            }
        }
        let hard_collisions = hard_collisions.finalize();
        if hard_collisions.is_none() {
            for sc in soft_collisions {
                sc.resolve()?;
            }
        }
        Ok(hard_collisions)
    }
}
