use std::error::Error;
use std::path::PathBuf;

pub const HELP: &str =
    "Usage: promo [-r: rename if collides] [-f: overwrite] [-t: target DIR] [DIR(S)]";

pub struct Args {
    pub rename: bool,
    pub overwrite: bool,
    pub target: Option<PathBuf>,
    pub promdirs: Vec<PathBuf>,
}

pub fn parse_args() -> Result<Args, Box<dyn Error>> {
    use lexopt::prelude::*;
    let mut parser = lexopt::Parser::from_env();
    let mut rename = false;
    let mut overwrite = false;
    let mut target = None;
    let mut promdirs: Vec<PathBuf> = Vec::new();
    while let Some(arg) = parser.next()? {
        match arg {
            Short('r') => rename = true,
            Short('f') => overwrite = true,
            Short('t') => {
                let dir: PathBuf = parser.value()?.into();
                if !dir.is_dir() {
                    return Err(format!("No such target directory: {}", dir.display()).into());
                }
                target = Some(dir.canonicalize().unwrap());
            }
            Short('h') => return Err(HELP.into()),
            Value(val) => {
                let mut dir: PathBuf = val.into();
                if !dir.is_dir() {
                    return Err(format!("No such directory: {}", dir.display()).into());
                }
                dir = dir.canonicalize().unwrap();
                promdirs.push(dir);
            }
            _ => return Err(Box::new(arg.unexpected())),
        }
    }

    Ok(Args {
        rename,
        overwrite,
        target,
        promdirs,
    })
}
