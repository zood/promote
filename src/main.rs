#![feature(let_chains)]

mod cli;
mod collisions;
mod mover;

use std::{
	borrow::Cow,
	io,
	path::{Path, PathBuf},
	sync::LazyLock,
};

use mover::Mover;
use promote::Pedigree;

struct App;
impl App {
	fn die(self) -> ! {
		std::process::exit(1);
	}
	fn info(self, msg: impl std::fmt::Display) -> Self {
		eprintln!("[promote] {msg}");
		self
	}
	fn err(self, msg: impl std::fmt::Display) -> Self {
		eprintln!("[promote] ERROR: {msg}");
		self
	}
}

static PWD: LazyLock<PathBuf> =
	LazyLock::new(|| std::env::current_dir().unwrap_or_else(|_| App.err("invalid PWD").die()));

fn pick_scenario<'a: 'b, 'b>(
	promdirs: &'a [PathBuf],
	target: &'b Option<PathBuf>,
) -> (Cow<'a, [PathBuf]>, &'b Path) {
	if let Some(target) = target {
		if promdirs.is_empty() {
			if target.is_ancestor_or_sibling_of(&PWD) {
				return (Cow::from(vec![PWD.to_path_buf()]), target);
			}
		} else if promdirs
			.iter()
			.all(|pd| promote::nonroot_common_ancestor(target, pd).is_some())
		{
			return (Cow::from(promdirs), target);
		}
	} else if promdirs.is_empty() {
		let parent = PWD
			.parent()
			.unwrap_or_else(|| App.err("no parent for the PWD").die());
		return (Cow::from(vec![PWD.to_path_buf()]), parent);
	} else if promdirs.len() == 1 {
		if PWD.is_ancestor_of(&promdirs[0]) {
			return (Cow::from(promdirs), &PWD);
		} else if promdirs[0].is_ancestor_of(&PWD) {
			return (Cow::from(vec![PWD.to_path_buf()]), &promdirs[0]);
		}
	} else if promdirs.len() > 1 && promdirs.iter().all(|pd| PWD.is_ancestor_of(pd)) {
		return (Cow::from(promdirs), &PWD);
	}
	App.err("unsupported scenario").die();
}

fn main() -> io::Result<()> {
	let cli::Args {
		rename,
		overwrite,
		target,
		mut promdirs,
	} = match cli::parse_args() {
		Ok(v) => v,
		Err(e) => App.err(&e).die(),
	};

	promdirs.sort_by_key(|path| std::cmp::Reverse(path.components().count()));
	let (promdirs, target) = pick_scenario(&promdirs, &target);
	let mover = Mover::new(rename, overwrite);
	if let Some(collisions) = mover.relocate(&promdirs, target)? {
		App.info(format!("COLLISIONS:\n{}", collisions)).die();
	} else {
		println!("{}", target.display());
	}
	Ok(())
}
