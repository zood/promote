#![feature(let_chains)]

use std::path::{Path, PathBuf};

pub trait Pedigree {
    fn is_ancestor_of(&self, other: &Path) -> bool;
    fn is_ancestor_or_sibling_of(&self, other: &Path) -> bool;
}

impl Pedigree for Path {
    fn is_ancestor_of(&self, other: &Path) -> bool {
        self != other && other.starts_with(self)
    }

    fn is_ancestor_or_sibling_of(&self, other: &Path) -> bool {
        if self == other {
            return false;
        } else if other.starts_with(self) {
            return true;
        }
        match (self.parent(), other.parent()) {
            (Some(a), Some(b)) => a == b,
            _ => false,
        }
    }
}

pub fn nonroot_common_ancestor(p1: &Path, p2: &Path) -> Option<PathBuf> {
    let mut ancestor = PathBuf::new();
    let mut p1_comps = p1.iter();
    let mut p2_comps = p2.iter();
    loop {
        if let Some(p1c) = p1_comps.next()
            && p2_comps.next() == Some(p1c)
        {
            ancestor.push(p1c);
        } else {
            break;
        }
    }
    if ancestor == Path::new("/") {
        return None;
    }
    Some(ancestor)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_nonroot_common_ancestor1() {
        let p1 = Path::new("/a/b/c/d");
        let p2 = Path::new("/a/b/1/2");
        assert_eq!(nonroot_common_ancestor(p1, p2), Some(PathBuf::from("/a/b")));
    }

    #[test]
    fn test_nonroot_common_ancestor2() {
        let p1 = Path::new("/a/b");
        let p2 = Path::new("/a/b/1/2");
        assert_eq!(nonroot_common_ancestor(p1, p2), Some(PathBuf::from("/a/b")));
    }

    #[test]
    fn test_nonroot_common_ancestor3() {
        let p1 = Path::new("/1/2");
        let p2 = Path::new("/a/b/c");
        assert_eq!(nonroot_common_ancestor(p1, p2), None);
    }

    #[test]
    fn test_is_ancestor_of() {
        let p1 = Path::new("/a/b");
        let p2 = Path::new("/a/b/1/2");
        let p3 = Path::new("/a/b/");
        assert!(p1.is_ancestor_of(p2));
        assert!(!p1.is_ancestor_of(p1));
        assert!(!p3.is_ancestor_of(p1));
    }

    #[test]
    fn test_is_ancestor_or_sibling_of() {
        let p1 = Path::new("/a/a");
        let p2 = Path::new("/a/b/");
        let p3 = Path::new("/b/a");
        assert!(p1.is_ancestor_or_sibling_of(p2));
        assert!(!p1.is_ancestor_or_sibling_of(p3));
        assert!(!p1.is_ancestor_or_sibling_of(p1));
    }
}
