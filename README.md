#### Promote the content of the directory into a sibling or a parent directory

    Usage: promo [-r: rename if collides] [-f: overwrite] [-t: target DIR] [DIR(S)]
